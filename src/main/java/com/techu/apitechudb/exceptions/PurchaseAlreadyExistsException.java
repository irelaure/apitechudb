package com.techu.apitechudb.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.CONFLICT)
public class PurchaseAlreadyExistsException extends RuntimeException {
    public PurchaseAlreadyExistsException (String id) {
        super ("Purchase with id " + id + " already exists");
    }
}