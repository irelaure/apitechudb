package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.PurchaseModel;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
@Document(collection = "purchases")
public interface PurchaseRepository extends MongoRepository<PurchaseModel, String> {
}
