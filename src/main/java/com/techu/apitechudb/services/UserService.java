package com.techu.apitechudb.services;

import com.techu.apitechudb.exceptions.UserAlreadyExistsException;
import com.techu.apitechudb.exceptions.UserNotFoundException;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public List<UserModel> findAll(String orderBy) {
        return !isNull(orderBy) ? this.userRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy)):
                this.userRepository.findAll();
    }

    public UserModel findById(String id) throws UserNotFoundException {
        return this.userRepository.findById(id).orElseThrow(() -> new UserNotFoundException(id));
    }

    public UserModel createUser(UserModel user) throws UserAlreadyExistsException {
        this.userRepository
                .findById(user.getId())
                .ifPresent (s -> {
                        throw new UserAlreadyExistsException(user.getId());
                }
        );
        return this.userRepository.save(user);
    }

    public UserModel updateUser(String id,UserModel newUser) throws UserNotFoundException {
        this.deleteUser(id);
        return this.userRepository.save(newUser);
    }

    public UserModel patchUser(String id, UserModel newUser) throws UserNotFoundException {
        UserModel user = this.findById(id);
        if (!isNull(newUser.getName())) {
            user.setName(newUser.getName());
        }
        if (!isNull(newUser.getAge())) {
            user.setAge(newUser.getAge());
        }
        return this.userRepository.save(user);
    }

    public void deleteUser(String id) throws UserNotFoundException {
        this.findById(id);
        userRepository.deleteById(id);
    }
}
