package com.techu.apitechudb.exceptions;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND)
public class PurchaseNotFoundException extends Exception {
    public PurchaseNotFoundException (String id) {
        super ("Purchase with id " + id + " not found");
    }
}
