package com.techu.apitechudb.controllers;

import com.techu.apitechudb.exceptions.PurchaseAlreadyExistsException;
import com.techu.apitechudb.exceptions.PurchaseNotFoundException;
import com.techu.apitechudb.exceptions.UserNotFoundException;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.services.PurchaseService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2/purchases")
public class PurchaseController {

    @Autowired
    private PurchaseService purchaseService;

    @GetMapping
    public ResponseEntity<List<PurchaseModel>> getPurchases(@RequestParam(name="$orderBy", required = false) String orderBy) {
        return new ResponseEntity<>(this.purchaseService.findAll(orderBy), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<PurchaseModel> getPurchase(@PathVariable String id) throws PurchaseNotFoundException {
        return new ResponseEntity<>(this.purchaseService.findById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<PurchaseModel> addPurchase(@RequestBody PurchaseModel purchaseModel) throws UserNotFoundException, PurchaseAlreadyExistsException {
        return new ResponseEntity<>(this.purchaseService.createPurchase(purchaseModel), HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deletePurchase(@PathVariable String id) throws PurchaseNotFoundException {
        this.purchaseService.deletePurchase(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
