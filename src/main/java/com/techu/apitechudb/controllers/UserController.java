package com.techu.apitechudb.controllers;

import com.techu.apitechudb.exceptions.UserAlreadyExistsException;
import com.techu.apitechudb.exceptions.UserNotFoundException;
import com.techu.apitechudb.models.UserModel;
import com.techu.apitechudb.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2/users")
public class UserController {

    @Autowired
    private UserService userService;

    @GetMapping
    public ResponseEntity<List<UserModel>> getUsers(@RequestParam(name="$orderBy", required = false) String orderBy) {
        return new ResponseEntity<>(this.userService.findAll(orderBy), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<UserModel> getUser(@PathVariable String id) throws UserNotFoundException {
        return new ResponseEntity<>(this.userService.findById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<UserModel> addUser(@RequestBody UserModel userModel) throws UserAlreadyExistsException {
        return new ResponseEntity<>(this.userService.createUser(userModel), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<UserModel> updateUser(@PathVariable String id, @RequestBody UserModel user)
            throws UserNotFoundException {
        return new ResponseEntity<>(this.userService.updateUser(id, user), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<UserModel> patchUser(@PathVariable String id,
                               @RequestBody UserModel prod) throws UserNotFoundException {
        return new ResponseEntity<>(this.userService.patchUser(id, prod), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteUser(@PathVariable String id) throws UserNotFoundException {
        this.userService.deleteUser(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
