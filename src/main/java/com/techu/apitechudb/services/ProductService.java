package com.techu.apitechudb.services;

import com.techu.apitechudb.exceptions.ProductAlreadyExistsException;
import com.techu.apitechudb.exceptions.ProductNotFoundException;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.repositories.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;

import static java.util.Objects.isNull;

@Service
public class ProductService {

    @Autowired
    private ProductRepository productRepository;

    public List<ProductModel> findAll(String orderBy) {
        return !isNull(orderBy) ? this.productRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy)):
                this.productRepository.findAll();
    }

    public ProductModel findById(String id) throws ProductNotFoundException {
        return this.productRepository.findById(id).orElseThrow(() -> new ProductNotFoundException(id));
    }

    public ProductModel createProduct(ProductModel product) throws ProductAlreadyExistsException {
        this.productRepository
                .findById(product.getId())
                .ifPresent (s -> {
                            throw new ProductAlreadyExistsException(product.getId());
                        }
                );
        return this.productRepository.save(product);
    }

    public ProductModel updateProduct(String id,ProductModel newProduct) throws ProductNotFoundException {
        this.deleteProduct(id);
        return this.productRepository.save(newProduct);
    }

    public ProductModel patchProduct(String id, ProductModel newProduct) throws ProductNotFoundException {
        ProductModel product = this.findById(id);
        if (!isNull(newProduct.getDesc())) {
            product.setDesc(newProduct.getDesc());
        }
        if (!isNull(newProduct.getPrice())) {
            product.setPrice(newProduct.getPrice());
        }
        return this.productRepository.save(product);
    }

    public void deleteProduct(String id) throws ProductNotFoundException {
        this.findById(id);
        productRepository.deleteById(id);
    }
}
