package com.techu.apitechudb.controllers;

import com.techu.apitechudb.exceptions.ProductNotFoundException;
import com.techu.apitechudb.models.ProductModel;
import com.techu.apitechudb.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/apitechu/v2/products")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping
    public ResponseEntity<List<ProductModel>> getProducts(@RequestParam(name="$orderBy", required = false) String orderBy) {
        return new ResponseEntity<>(this.productService.findAll(orderBy), HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<ProductModel> getProduct(@PathVariable String id) throws ProductNotFoundException {
        return new ResponseEntity<>(this.productService.findById(id), HttpStatus.OK);
    }

    @PostMapping
    public ResponseEntity<ProductModel> addProduct(@RequestBody ProductModel productModel) {
        return new ResponseEntity<>(this.productService.createProduct(productModel), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<ProductModel> updateProduct(@PathVariable String id, @RequestBody ProductModel product)
            throws ProductNotFoundException {
        return new ResponseEntity<>(this.productService.updateProduct(id, product), HttpStatus.OK);
    }

    @PatchMapping("/{id}")
    public ResponseEntity<ProductModel> patchProduct(@PathVariable String id,
                                     @RequestBody ProductModel prod) throws ProductNotFoundException {
        return new ResponseEntity<>(this.productService.patchProduct(id, prod), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteProduct(@PathVariable String id) throws ProductNotFoundException {
        this.productService.deleteProduct(id);
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
