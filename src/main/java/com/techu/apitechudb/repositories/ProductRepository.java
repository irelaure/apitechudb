package com.techu.apitechudb.repositories;

import com.techu.apitechudb.models.ProductModel;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
@Document(collection = "products")
public interface ProductRepository extends MongoRepository<ProductModel, String> {
}
