package com.techu.apitechudb.services;

import com.techu.apitechudb.exceptions.ProductNotFoundException;
import com.techu.apitechudb.exceptions.PurchaseAlreadyExistsException;
import com.techu.apitechudb.exceptions.PurchaseNotFoundException;
import com.techu.apitechudb.exceptions.UserNotFoundException;
import com.techu.apitechudb.models.PurchaseModel;
import com.techu.apitechudb.repositories.PurchaseRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Map;

import static java.util.Objects.isNull;

@Service
public class PurchaseService {

    @Autowired
    private PurchaseRepository purchaseRepository;

    @Autowired
    private UserService userService;

    @Autowired
    private ProductService productService;

    public List<PurchaseModel> findAll(String orderBy) {
        return !isNull(orderBy) ? this.purchaseRepository.findAll(Sort.by(Sort.Direction.ASC, orderBy)):
                this.purchaseRepository.findAll();
    }

    public PurchaseModel findById(String id) throws PurchaseNotFoundException {
        return this.purchaseRepository.findById(id).orElseThrow(() -> new PurchaseNotFoundException(id));
    }

    public PurchaseModel createPurchase(PurchaseModel purchase) throws PurchaseAlreadyExistsException, UserNotFoundException {
        this.userService.findById(purchase.getUserId());
        this.purchaseRepository
                .findById(purchase.getId())
                .ifPresent (s -> {
                            throw new PurchaseAlreadyExistsException(purchase.getId());
                        }
                );
        purchase.setAmount(getTotalPrice(purchase.getPurchaseItems()));
        return this.purchaseRepository.save(purchase);
    }

    public Float getTotalPrice(Map<String, Integer> purchaseItems) throws PurchaseAlreadyExistsException, ProductNotFoundException {
        return (float)purchaseItems.entrySet().stream().mapToDouble(x -> {
            this.productService.findById(x.getKey());
            return this.productService.findById(x.getKey()).getPrice()* x.getValue();
        }).sum();
    }

    public void deletePurchase(String id) throws PurchaseNotFoundException {
        this.findById(id);
        purchaseRepository.deleteById(id);
    }
}
